package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.TransferService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication

public class NiceBankApplication{

	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}

	@ConditionalOnProperty(prefix = "job.autorun", name = "enabled", havingValue = "true", matchIfMissing = true)
	@Bean
	CommandLineRunner commandLineRunner(TransferService transferService){
		return args -> {
			Utilisateur utilisateur1=transferService.saveUtilisateur("user1","last1",new Date(),"first1","Male");

			Utilisateur utilisateur2=transferService.saveUtilisateur("user2","last2",new Date(),"first2","Feale");

			Compte compte1=transferService.saveCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

			Compte compte2=transferService.saveCompte("010000B025001000","RIB2",BigDecimal.valueOf(140000L),utilisateur2);

			transferService.saveTransfer(BigDecimal.TEN,compte1,compte2,new Date(),"motif");
		};
	}

}
