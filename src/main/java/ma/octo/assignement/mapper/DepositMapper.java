package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDTO;

public class DepositMapper {
    private DepositMapper(){}

    public static DepositDTO fromDeposit(MoneyDeposit deposit) {
        DepositDTO depositDTO;
        depositDTO = new DepositDTO();
        depositDTO.setRib(deposit.getCompteBeneficiaire().getRib());
        depositDTO.setMontant(deposit.getMontant());
        depositDTO.setDate(deposit.getDateExecution());
        return depositDTO;
    }
}
