package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditTransfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditTransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditServiceImpl implements AuditService{
    Logger logger = LoggerFactory.getLogger(AuditServiceImpl.class);
    private final AuditTransferRepository auditTransferRepository;

    public AuditServiceImpl(AuditTransferRepository auditTransferRepository) {
        this.auditTransferRepository = auditTransferRepository;
    }

    @Override
    public void auditTransfer(String message) {

        logger.info("Audit de l'événement {}", EventType.TRANSFER);

        AuditTransfer audit = new AuditTransfer();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage(message);
        auditTransferRepository.save(audit);
    }

    @Override
    public void auditDeposit(String message) {

        logger.info("Audit de l'événement {}", EventType.DEPOSIT);

        AuditTransfer audit = new AuditTransfer();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage(message);
        auditTransferRepository.save(audit);
    }
}

