package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface TransferService {
    void newTransfer(String nrCompteEmetteur,String nrCompteBeneficiaire,String motif,BigDecimal montant) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;
    List<Transfer> listTransfer();
    Transfer getTransfer(Long id);
    List<Compte> listCompte();
    List<Utilisateur> listUtilisateur();
    Transfer saveTransfer(BigDecimal decimal, Compte beneficiaire, Compte emetteur, Date date, String motif);
    void deleteTransfer(Long id);
    Compte saveCompte(String nrCompte,String rib,BigDecimal montant,Utilisateur user);
    Utilisateur saveUtilisateur(String username,String lastname,Date date,String firstName,String gender);
}
