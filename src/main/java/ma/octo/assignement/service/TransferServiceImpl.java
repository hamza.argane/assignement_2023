package ma.octo.assignement.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class TransferServiceImpl implements TransferService{

    public static final int MONTANT_MAXIMAL = 10000;

    Logger logger = LoggerFactory.getLogger(TransferServiceImpl.class);
    @NonNull
    private CompteRepository compteRepository;

    @NonNull
    private TransferRepository transferRepository;

    @NonNull
    private AuditService monservice;

    @NonNull
    private UtilisateurRepository utilisateurRepository;

    @Override
    public void newTransfer(String nrCompteEmetteur,String nrCompteBeneficiaire,String motif,BigDecimal montant) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException{
        Compte compte1 = compteRepository.findByNrCompte(nrCompteEmetteur);
        Compte compte2 = compteRepository.findByNrCompte(nrCompteBeneficiaire);

        if (compte1 == null || compte2 == null) {
            logger.info("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (montant.intValue() == 0) {
            logger.info("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (montant.intValue() < 10) {
            logger.info("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (montant.intValue() > MONTANT_MAXIMAL) {
            logger.info("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }
        if (motif.length() == 0) {
            logger.info("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (compte1.getSolde().intValue() - montant.intValue() < 0) {
            logger.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant");
        }


        compte1.setSolde(compte1.getSolde().subtract(montant));
        compteRepository.save(compte1);

        compte2.setSolde(compte2.getSolde().add(montant));
        compteRepository.save(compte2);

        saveTransfer(montant,compte2,compte1,new Date(),"motif");

        monservice.auditTransfer("Transfer depuis " + nrCompteEmetteur + " vers " + nrCompteBeneficiaire
                + " d'un montant de " + montant);
    }

    @Override
    public List<Transfer> listTransfer() {
        logger.info("Lister des transferts");
        return transferRepository.findAll();
    }

    @Override
    public Transfer getTransfer(Long id) {
        logger.info("Lister le transfert {}",id);
        return transferRepository.findById(id).orElse(null);
    }

    @Override
    public List<Compte> listCompte() {
        logger.info("Lister des comptes");
        return compteRepository.findAll();
    }

    @Override
    public List<Utilisateur> listUtilisateur() {
        logger.info("Lister des utilisateurs");
        return utilisateurRepository.findAll();
    }

    @Override
    public Transfer saveTransfer(BigDecimal decimal,Compte beneficiaire,Compte emetteur,Date date,String motif) {
        Transfer transfer = new Transfer();
        transfer.setMontantTransfer(decimal);
        transfer.setCompteBeneficiaire(beneficiaire);
        transfer.setCompteEmetteur(emetteur);
        transfer.setDateExecution(date);
        transfer.setMotifTransfer(motif);
        transferRepository.save(transfer);
        return transfer;
    }

    @Override
    public void deleteTransfer(Long id) {
        transferRepository.deleteById(id);
    }

    @Override
    public Compte saveCompte(String nrCompte, String rib, BigDecimal montant,Utilisateur user) {
        Compte compte = new Compte();
        compte.setNrCompte(nrCompte);
        compte.setRib(rib);
        compte.setSolde(montant);
        compte.setUtilisateur(user);

        compteRepository.save(compte);
        return compte;
    }

    @Override
    public Utilisateur saveUtilisateur(String username, String lastname,Date date, String firstName, String gender) {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setUsername(username);
        utilisateur.setLastname(lastname);
        utilisateur.setFirstname(firstName);
        utilisateur.setGender(gender);

        utilisateurRepository.save(utilisateur);
        return utilisateur;
    }
}
