package ma.octo.assignement.web;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDTO;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositRepository;
import ma.octo.assignement.service.AuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController(value = "/deposits")@RequiredArgsConstructor
public class DepositController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger logger = LoggerFactory.getLogger(DepositController.class);

    @NonNull
    MoneyDepositRepository depositRepository;

    @NonNull
    CompteRepository compteRepository;

    @NonNull
    private AuditService monservice;

    @PostMapping("/executerDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeposit(@RequestBody DepositDTO depositDTO)
            throws CompteNonExistantException, DepositException {
        Compte c = compteRepository.findByrib(depositDTO.getRib());

        if (c == null) {
            logger.info("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (depositDTO.getMontant().intValue() == 0F) {
            logger.info("Montant vide");
            throw new DepositException("Montant vide");
        } else if (depositDTO.getMontant().intValue() < 10) {
            logger.info("Montant minimal de transfer non atteint");
            throw new DepositException("Montant minimal de transfer non atteint");
        } else if (depositDTO.getMontant().intValue() > MONTANT_MAXIMAL) {
            logger.info("Montant maximal de transfer dépassé");
            throw new DepositException("Montant maximal de transfer dépassé");
        }


        c.setSolde(c.getSolde().add(new BigDecimal(depositDTO.getMontant().toString())));
        compteRepository.save(c);


        MoneyDeposit deposit = new MoneyDeposit();
        deposit.setDateExecution(depositDTO.getDate());
        deposit.setCompteBeneficiaire(compteRepository.findByrib(c.getRib()));
        deposit.setMontant(depositDTO.getMontant());

        depositRepository.save(deposit);

        monservice.auditTransfer("Transfer vers " + depositDTO
                .getRib() + " d'un montant de " + depositDTO.getMontant()
                .toString());
    }
}
