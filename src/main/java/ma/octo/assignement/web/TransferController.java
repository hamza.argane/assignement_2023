package ma.octo.assignement.web;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/transfers")@RequiredArgsConstructor
class TransferController {



    @NonNull TransferServiceImpl transferService;


    @GetMapping("/listDesTransferts")
    List<Transfer> loadAll() {
        return transferService.listTransfer();
    }

    @GetMapping("/listDesTransferts/{id}")
    Transfer loadById(@PathVariable Long id) {
        return transferService.getTransfer(id);
    }
    @GetMapping("listOfAccounts")
    List<Compte> loadAllCompte() {
        return transferService.listCompte();
    }

    @GetMapping("listUtilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        return transferService.listUtilisateur();
    }

    @DeleteMapping("delete/{id}")
    void deleteTransfer(@PathVariable Long id){
        transferService.deleteTransfer(id);
    }

    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException{
       transferService.newTransfer(transferDto.getNrCompteEmetteur(), transferDto.getNrCompteBeneficiaire(), transferDto.getMotif(),transferDto.getMontant());
    }
}
