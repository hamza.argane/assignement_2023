package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.TransferService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {"job.autorun.enabled=false"})
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class CompteRepositoryTest {

  @Autowired
  private TransferService transferService;

  @Autowired
  private CompteRepository compteRepository;

  @Test
  void findOne() {
    Utilisateur utilisateur1=transferService.saveUtilisateur("user1","last1",new Date(),"first1","Male");

    Compte compte1=transferService.saveCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

    assertEquals(compte1,compteRepository.getById(compte1.getId()));
  }


  @Test
  void findAll() {
    Utilisateur utilisateur1=transferService.saveUtilisateur("user3","last1",new Date(),"first1","Male");

    Utilisateur utilisateur2=transferService.saveUtilisateur("user4","last2",new Date(),"first2","Female");

    List<Compte> compte=new ArrayList<>();

    compte.add(transferService.saveCompte("010000A000001001","RIB3",BigDecimal.valueOf(200000L),utilisateur1));

    compte.add(transferService.saveCompte("010000B025001001","RIB4",BigDecimal.valueOf(140000L),utilisateur2));

    assertEquals(compte.size(),compteRepository.findAll().size());
  }

  @Test
  void save() {
    Utilisateur utilisateur1=transferService.saveUtilisateur("user1","last1",new Date(),"first1","Male");

    Compte compte1=transferService.saveCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

    assertNotNull(compteRepository.findById(compte1.getId()));
  }

  @Test
  void delete() {
    Utilisateur utilisateur1=transferService.saveUtilisateur("user1","last1",new Date(),"first1","Male");

    Compte compte1=transferService.saveCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

    compteRepository.delete(compte1);

    assertNull(compteRepository.findById(compte1.getId()).orElse(null));
  }
}