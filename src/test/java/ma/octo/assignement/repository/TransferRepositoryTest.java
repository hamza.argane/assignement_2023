package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.TransferService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {"job.autorun.enabled=false"})
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class TransferRepositoryTest {

  @Autowired
  private TransferService transferService;

  @Test
  void findOne() {
   Utilisateur utilisateur1=transferService.saveUtilisateur("user1","last1",new Date(),"first1","Male");

    Utilisateur utilisateur2=transferService.saveUtilisateur("user2","last2",new Date(),"first2","Female");

    Compte compte1=transferService.saveCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

    Compte compte2=transferService.saveCompte("010000B025001000","RIB2",BigDecimal.valueOf(140000L),utilisateur2);

    Transfer transfer = transferService.saveTransfer(BigDecimal.TEN,compte1,compte2,new Date(),"motif");

    assertEquals(transferService.getTransfer(transfer.getId()),transfer);
  }


  @Test
  void findAll() {
    Utilisateur utilisateur1=transferService.saveUtilisateur("user3","last1",new Date(),"first1","Male");

    Utilisateur utilisateur2=transferService.saveUtilisateur("user4","last2",new Date(),"first2","Female");

    Compte compte1=transferService.saveCompte("010000A000001001","RIB3",BigDecimal.valueOf(200000L),utilisateur1);

    Compte compte2=transferService.saveCompte("010000B025001001","RIB4",BigDecimal.valueOf(140000L),utilisateur2);
    List<Transfer> transfers=new ArrayList<>();


    transfers.add( transferService.saveTransfer(BigDecimal.TEN,compte1,compte2,new Date(),"motif"));
    transfers.add( transferService.saveTransfer(BigDecimal.TEN,compte1,compte2,new Date(),"motif"));

    List<Transfer> listTransfert=transferService.listTransfer();


     assertEquals(transfers.size(),listTransfert.size());
  }

  @Test
 void save() {
    Utilisateur utilisateur1=transferService.saveUtilisateur("user1","last1",new Date(),"first1","Male");

    Utilisateur utilisateur2=transferService.saveUtilisateur("user2","last2",new Date(),"first2","Female");

    Compte compte1=transferService.saveCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

    Compte compte2=transferService.saveCompte("010000B025001000","RIB2",BigDecimal.valueOf(140000L),utilisateur2);

    Transfer transfer = transferService.saveTransfer(BigDecimal.TEN,compte1,compte2,new Date(),"motif");

    Long id=transfer.getId();
    assertNotNull(transferService.getTransfer(id));
  }

  @Test
  void delete() {
    Utilisateur utilisateur1=transferService.saveUtilisateur("user1","last1",new Date(),"first1","Male");

    Utilisateur utilisateur2=transferService.saveUtilisateur("user2","last2",new Date(),"first2","Female");

    Compte compte1=transferService.saveCompte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);

    Compte compte2=transferService.saveCompte("010000B025001000","RIB2",BigDecimal.valueOf(140000L),utilisateur2);

    Transfer transfer = transferService.saveTransfer(BigDecimal.TEN,compte1,compte2,new Date(),"motif");

    Long id=transfer.getId();

    transferService.deleteTransfer(id);

    assertNull(transferService.getTransfer(id));
  }
}